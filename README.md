# Code Dojo 35 - A List apart
This is a worked example from the [35th London Code Dojo](http://www.meetup.com/London-Code-Dojo/events/228523030/). Feel free to play around with it.

The source of the kata is the Lists Kata, put simply:

Create three implementations of a simple list data structure:
1. A singly linked list
1. A doubly linked list
1. A list with no form of linking

Each node has:
* Metadata: e.g. link to next node (Metadata can contain anything that you need to make the list work)
* Data: a string payload (Data contains only the string payload)

Objectives:
* A node can be added to the end of the list
* Any node can be removed from the list
* The list can be asked to return all the nodes' values as an array
* The list can be searched for a payload, if found it returns the node that contains that payload

The earliest mention I could find of it was from Dave Thomas' list of coding katas [here](http://codekata.com/kata/kata21-simple-lists/)

You can find out more about the London Code Dojo at our [homepage](http://www.meetup.com/London-Code-Dojo/).
